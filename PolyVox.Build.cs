using UnrealBuildTool;

public class PolyVox : ModuleRules
{
    public PolyVox(TargetInfo Target)
    {
        Type = ModuleType.External;

        //TODO: Need absolute path?
        string PolyVoxPath = "Libraries/PolyVox/";

        PublicIncludePaths.Add(PolyVoxPath + "include");
    }
}
